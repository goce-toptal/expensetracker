package com.example.expensestrackeractionbar.ui;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.dom.Expenses;
import com.example.expensestrackeractionbar.utility.MyDateFormatter;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ViewExpensesActivity extends Activity {

	TextView comment, ammount, description,date,time;
	RadioGroup radioType;
	Button edit;
	String username, password, email;
	String expensesId;
	Expenses expense;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_expense);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		initView();
		
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			expensesId = bundle.getString(Expenses.OBJECT_ID);
			if (!Utility.HaveNetworkConnection(this)) {
				Utility u = new Utility(this);
				u.createInternetDisabledAlert();
				return;
			}
			getCloudExpense();
		}
	}
	
	public void getCloudExpense(){
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(Expenses.TABBLE_NAME);
		query.getInBackground(expensesId, new GetCallback<ParseObject>() {
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					expense = Expenses.mapExpensesObjectToClass(object);
					comment.setText(expense.getComent());
					description.setText("Description:\n\n"+expense.getDescription());
					ammount.setText("Ammount:\n"+expense.getAmount() + "$");
					String defaultdate = MyDateFormatter.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat().format(expense.getDate());
					String defaulttime = MyDateFormatter.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat().format(expense.getDate());
					date.setText(""+defaultdate);
					time.setText(""+defaulttime);
					((RadioButton) radioType.getChildAt(expense.getType())).setChecked(true);
				} else {
					
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void initView() {
		comment = (TextView) findViewById(R.id.comment);
		ammount = (TextView) findViewById(R.id.ammount);
		description = (TextView) findViewById(R.id.description);
		date = (TextView) findViewById(R.id.dateEdit);
		time = (TextView) findViewById(R.id.timeEdit);
	//

		radioType = (RadioGroup) findViewById(R.id.radioType);
		radioType.setVisibility(RadioGroup.VISIBLE);
		Calendar c = Calendar.getInstance();
		String defaultdate = MyDateFormatter.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat().format(c.getTime());
		String defaulttime = MyDateFormatter.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat().format(c.getTime());
		date.setText(defaultdate);
		time.setText(defaulttime);
	}

}
