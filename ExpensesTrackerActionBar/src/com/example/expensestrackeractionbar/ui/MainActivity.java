package com.example.expensestrackeractionbar.ui;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.Spinner;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.ui.AddType.AddTypeCalback;
import com.example.expensestrackeractionbar.ui.ExpensesListFragment.GetAllEpensessAsinq;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;

public class MainActivity extends FragmentActivity  implements ActionBar.TabListener,ViewPager.OnPageChangeListener,AddTypeCalback{
	
	private String[] tabs = { "Balance","Expenses","Income" };
	private ViewPager viewPager;
	private ActionBar actionBar;

	GetAllEpensessAsinq getAllCloudExpenses;
	View listtop;
	//
	Spinner spinnerTime;
	Spinner spinnerMonth;
	
	CalendarView calendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        actionBar = getActionBar();
    	setupViewPager();
	    setupTabs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if(item.getItemId() == R.id.action_logout){
        	Utility.userLogout(this);
        	this.finish();
        	return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void setupTabs() {
    	
    	for (String tab_name : tabs) {
    		actionBar.addTab(actionBar.newTab().setText(tab_name)
                    .setTabListener(this));
    	}
    }
    
    private void setupViewPager(){
    	setViewPager((ViewPager) findViewById(R.id.pager));
    	MainPagerAdapter viewPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
    	getViewPager().setAdapter(viewPagerAdapter);
    	getViewPager().setOnPageChangeListener(this);
    }
    
    /*Tabs override methods*/
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		getViewPager().setCurrentItem(tab.getPosition());
	}


	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	
	/*view pager override methods*/
	
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub
		getActionBar().selectTab(getActionBar().getTabAt(arg0));
	}
	
	public class MainPagerAdapter extends FragmentPagerAdapter {

		int pos;

		public MainPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return tabs[position];
		}

		//
		@Override
		public int getCount() {
			return tabs.length;
		}


		@Override
		public Fragment getItem(int position) {
			Fragment f;
			switch (position) {
			case 0:
				f = ExpensesListFragment.newInstance("");
				break;
			case 1:
				f = AddExpenses.newInstance("");
				break;
			case 2:
				f = AddIncome.newInstance("");
				break;
			default:
				f = ExpensesListFragment.newInstance("");
				break;
			}
			
			return f;
		}
	}
		
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		StaticData.getInstance().setExpenseListEdited(true);
	}

	public ViewPager getViewPager() {
		return viewPager;
	}

	public void setViewPager(ViewPager viewPager) {
		this.viewPager = viewPager;
	}

	@Override
	public void updatedExpenseList() {
		// TODO Auto-generated method stub
		StaticData.getInstance().setExpenseListEdited(true);
		viewPager.setCurrentItem(0);
		
	}

}
