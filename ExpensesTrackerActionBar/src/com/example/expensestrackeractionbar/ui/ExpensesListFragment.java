package com.example.expensestrackeractionbar.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.echo.holographlibrary.Bar;
import com.echo.holographlibrary.BarGraph;
import com.echo.holographlibrary.BarGraph.OnBarClickedListener;
import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.adapters.ExpensesAdapter;
import com.example.expensestrackeractionbar.dom.Expenses;
import com.example.expensestrackeractionbar.utility.Constants;
import com.example.expensestrackeractionbar.utility.ExpenseObservable;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * A placeholder fragment containing a simple view.
 */

public class ExpensesListFragment extends Fragment implements Observer{

	private View rootView;
	private ExpensesAdapter adapter;
	private List<ParseObject> parseExspenses = new LinkedList<ParseObject>();
	public LinkedList<Expenses> expenses = new LinkedList<Expenses>();
	private GetAllEpensessAsinq expensesAsynq;
	private ListView listView;
	private View listtop;
	private Spinner spinnerTime;
	private Spinner spinnerType;
	private BarGraph barGraph;
	int totalIncome = 0;
	int totalExpense = 0;
	TextView totalExpenseEditText, totalIncomeEditText, totalBalance,averageAmount;
	public int FILTER_TYPE = 0;
	public int FILTER_YEAR = 2014;
	public int FILTER_MONTH = 1;
	public int FILTER_DAY = 1;

	public final int YEARLY_FILTER_TYPE = 0;
	public final int MONTHLY_FILTER_TYPE = 1;
	public final int WEEKLY_FILTER_TYPE = 2;

	final List<String> filterList_YWM = new ArrayList<String>();

	ArrayAdapter<String> dataAdapter_filterList_YWM;

	private boolean userTouchSpiner = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		Calendar c = Calendar.getInstance();

		FILTER_TYPE = 0;
		FILTER_YEAR = c.get(Calendar.YEAR);
		FILTER_MONTH = c.get(Calendar.MONTH);
		FILTER_DAY = c.get(Calendar.DAY_OF_MONTH);
		

	}

	public static ExpensesListFragment newInstance(String title) {
		ExpensesListFragment pageFragment = new ExpensesListFragment();
		Bundle bundle = new Bundle();
		bundle.putString("title", title);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_main, container, false);
		adapter = new ExpensesAdapter(rootView.getContext(), expenses);
		initView(rootView);
		listView.addHeaderView(listtop);
		listView.setAdapter(adapter);
		


		return rootView;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();

		try {
			if (expensesAsynq != null
					&& expensesAsynq.getStatus() == Status.RUNNING)
				expensesAsynq.cancel(true);
		} catch (Exception e) {

		}
		
		Constants.expenseObservable.deleteObserver(this);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (StaticData.getInstance().isExpenseListEdited()) {
			StaticData.getInstance().setExpenseListEdited(false);
			if (getActivity() != null) {
				getCloudExpenses(getActivity());
			}
		}
		Constants.expenseObservable.addObserver(this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Add your menu entries here
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		if (id == R.id.action_reload) {
			getCloudExpenses(getActivity());
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void setupTotalExpenses() {
		totalExpenseEditText.setText(totalExpense + "$");
		totalIncomeEditText.setText(totalIncome + "$");
		totalBalance.setText(totalIncome - totalExpense + "$");
		averageAmount.setText(setupAverageAmount());
	}

	public String setupAverageAmount() {
		double amount = 0.0;
		
		String print= "";
		switch (FILTER_TYPE) {
		case WEEKLY_FILTER_TYPE:
				amount = totalExpense / 7;
			print = "Average: "+amount+"$";//
			break;
		case MONTHLY_FILTER_TYPE:
			Calendar  c  =  Calendar.getInstance();
			c.set(Calendar.MONTH, FILTER_MONTH);
			int max = c.getActualMaximum(Calendar.DAY_OF_MONTH);			
			amount = totalExpense / max;
			print = "Average: "+amount+"$";
			break;
		case YEARLY_FILTER_TYPE:
			amount = totalExpense / 12;
			print = "Average: "+amount+"$";
			break;

		default:
			
			break;
		}
		print = "Average: "+amount+"$";
		return print;
	}

	public void initView(View v) {

		listView = (ListView) v.findViewById(R.id.listView1);

		LayoutInflater listInflater = LayoutInflater.from(v.getContext());
		listtop = listInflater.inflate(R.layout.expence_header, null);
		totalExpenseEditText = (TextView) listtop
				.findViewById(R.id.totalExpense);
		averageAmount = (TextView) listtop
				.findViewById(R.id.averageAmount);
		totalIncomeEditText = (TextView) listtop.findViewById(R.id.totalIncome);
		totalBalance = (TextView) listtop.findViewById(R.id.totalBalance);

		addItemsOnSpinner2(listtop);
		addItemsOnSpinner1(listtop);

		listView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub
				if (visibleItemCount == 0)
					return;
				if (firstVisibleItem == 0)
					listtop.setTranslationY(-listView.getChildAt(0).getTop() / 3);
			}
		});

	}

	public void initGraphView(int type, int year, int month, int day) {
		barGraph = (BarGraph) listtop.findViewById(R.id.bargraph);
		barGraph.setOnBarClickedListener(new OnBarClickedListener() {

			@Override
			public void onClick(int index) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onUp(int index) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onBarUp() {
				// TODO Auto-generated method stub
			}
		});

		switch (type) {
		case 0:
			yearlyGraph(year);
			break;
		case 1:
			monthlyGraph(year, month);
			break;
		case 2:
			weeklyGraph(year, month, day);
			break;
		}
	}

	class Bars {
		Bars(String name) {
			this.name = name;
		}

		int sum = 0;
		String name;
	}

	public void yearlyGraph(int year) {
		final ArrayList<Bar> aBars = new ArrayList<Bar>();
		barGraph.setBars(aBars);
		barGraph.setShowAxisLabel(true);
		barGraph.setShowAxis(false);
		barGraph.setShowPopup(true);

		ArrayList<Bars> bars = new ArrayList<Bars>();
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < 12; i++) {
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, i);
			cal.set(Calendar.DATE, 1);
			String month = new SimpleDateFormat("MMM").format(cal.getTime());
			bars.add(new Bars(month));
			cal.clear();
		}

		barGraph.setMinimumHeight(20);

		for (int j = 0; j < expenses.size(); j++) {
			String month = new SimpleDateFormat("MMM").format(expenses.get(j)
					.getDate());
			String expYear = new SimpleDateFormat("yyyy").format(expenses
					.get(j).getDate());
			for (int i = 0; i < bars.size(); i++) {
				if (Integer.valueOf(expYear) == year)
					if (month.trim().equals(bars.get(i).name.trim())
							&& expenses.get(j).getType() == 0) {
						bars.get(i).sum += expenses.get(j).getAmount();
					}
			}
		}

		for (int i = 0; i < bars.size(); i++) {
			Bar bar = new Bar();
			bar.setLabelColor(Color.rgb(207, 207, 207));
			bar.setColor(getActivity().getResources().getColor(R.color.green));
			bar.setSelectedColor(Color.rgb(207, 207, 207));
			bar.setName(bars.get(i).name);

			bar.setValue(bars.get(i).sum);

			aBars.add(bar);
		}
	}

	public void defaultFilterData(Context context) {
		if (!Utility.HaveNetworkConnection(context)) {
			Utility u = new Utility(context);
			u.createInternetDisabledAlert();
			try {
				if (expensesAsynq != null
						&& expensesAsynq.getStatus() == Status.RUNNING)
					expensesAsynq.cancel(true);
			} catch (Exception e) {

			}
			return;
		}
		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				Expenses.TABBLE_NAME);
		query.orderByDescending("_created_at");
		query.whereEqualTo(Expenses.USER, ParseUser.getCurrentUser());
		expensesAsynq = new GetAllEpensessAsinq(context, query);
		expensesAsynq.execute();
	}

	public void getCloudExpenses(Context context) {
		
		if (!Utility.HaveNetworkConnection(getActivity())) {
			Utility u = new Utility(getActivity());
			u.createInternetDisabledAlert();
			return;
		}

		switch (FILTER_TYPE) {
		case YEARLY_FILTER_TYPE:
			yearlyFilterData(context);
			break;
		case MONTHLY_FILTER_TYPE:
			montlyFilterData(context);
			break;
		case WEEKLY_FILTER_TYPE:
			weeklyFilterData(context);
			break;
		default:
			defaultFilterData(context);
			break;
		}

	}

	public void weeklyFilterData(Context context) {

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				Expenses.TABBLE_NAME);
		query.whereEqualTo(Expenses.USER, ParseUser.getCurrentUser());

		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, FILTER_DAY - 6);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		Log.d("WEEKLY_T", c.getTime().toGMTString());//

		Calendar c2 = Calendar.getInstance();
		c2.set(Calendar.DATE, FILTER_DAY);
		c2.set(Calendar.HOUR, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);

		Log.d("DATE_T", c.getTime().getTime() - 1416178790000d + "");
		query.whereGreaterThanOrEqualTo(Expenses.TIME, c.getTime().getTime());
		query.whereLessThanOrEqualTo(Expenses.TIME, c2.getTime().getTime());
		query.orderByDescending("_created_at");
		expensesAsynq = new GetAllEpensessAsinq(context, query);
		expensesAsynq.execute();
	}

	public void montlyFilterData(Context context) {

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				Expenses.TABBLE_NAME);
		query.whereEqualTo(Expenses.USER, ParseUser.getCurrentUser());

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MONTH, FILTER_MONTH);
		c.set(Calendar.DATE, 1);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		Calendar c2 = Calendar.getInstance();
		c2.set(Calendar.MONTH, FILTER_MONTH + 1);
		c2.set(Calendar.DATE, 0);
		c2.set(Calendar.HOUR, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);

		Log.d("DATE_T", c.getTime().getTime() - 1416178790000d + "");
		query.whereGreaterThanOrEqualTo(Expenses.TIME, c.getTime().getTime());
		query.whereLessThanOrEqualTo(Expenses.TIME, c2.getTime().getTime());
		query.orderByDescending("_created_at");
		expensesAsynq = new GetAllEpensessAsinq(context, query);
		expensesAsynq.execute();
	}

	public void yearlyFilterData(Context context) {

		ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
				Expenses.TABBLE_NAME);
		query.whereEqualTo(Expenses.USER, ParseUser.getCurrentUser());

		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, FILTER_YEAR);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.DATE, 0);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		Calendar c2 = Calendar.getInstance();
		c2.set(Calendar.YEAR, FILTER_YEAR + 1);
		c2.set(Calendar.MONTH, 0);
		c2.set(Calendar.DATE, 0);
		c2.set(Calendar.HOUR, 0);
		c2.set(Calendar.MINUTE, 0);
		c2.set(Calendar.SECOND, 0);

		Log.d("DATE_T", c.getTime().getTime() - 1416178790000d + "");
		query.whereGreaterThanOrEqualTo(Expenses.TIME, c.getTime().getTime());
		query.whereLessThanOrEqualTo(Expenses.TIME, c2.getTime().getTime());
		query.orderByDescending("_created_at");
		expensesAsynq = new GetAllEpensessAsinq(context, query);
		expensesAsynq.execute();

	}

	public void monthlyGraph(int year, int month) {
		final ArrayList<Bar> aBars = new ArrayList<Bar>();
		barGraph.setBars(aBars);
		barGraph.setShowAxisLabel(true);
		barGraph.setShowAxis(false);
		barGraph.setShowPopup(false);

		ArrayList<Bars> bars = new ArrayList<Bars>();
		Calendar cal = Calendar.getInstance();

		Log.d("MONTH_Y", cal.get(Calendar.MONTH) + "");
		cal.set(Calendar.DATE, 1);
		int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		for (int i = 0; i < days; i++) {
			int day = i + 1;
			Bars bm;
			// if (i == 0 || i == days - 1)
			bm = new Bars(day + "");
			// else
			// bm = new Bars("");
			// bm.sum = 10;
			bars.add(bm);
		}

		barGraph.setMinimumHeight(20);

		for (int j = 0; j < expenses.size(); j++) {
			String monthstr = new SimpleDateFormat("M").format(expenses.get(j)
					.getDate());
			String yearstr = new SimpleDateFormat("yyyy").format(expenses
					.get(j).getDate());
			String dayrstr = new SimpleDateFormat("dd").format(expenses.get(j)
					.getDate());
			// Log.d("FILTER_MONTH", FILTER_MONTH+" | "+monthstr);
			if (FILTER_MONTH == Integer.valueOf(monthstr) - 1
					&& FILTER_YEAR == Integer.valueOf(yearstr)) {
				for (int i = 0; i < bars.size(); i++) {
					int locday = i + 1;
					if (locday == Integer.valueOf(dayrstr))
						bars.get(i).sum += expenses.get(j).getAmount();
				}
			}

		}

		for (int i = 0; i < bars.size(); i++) {
			Bar bar = new Bar();
			bar.setLabelColor(Color.rgb(207, 207, 207));
			bar.setColor(getActivity().getResources().getColor(R.color.green));
			bar.setSelectedColor(Color.rgb(207, 207, 207));
			bar.setName(bars.get(i).name);
			bar.setValue(bars.get(i).sum);

			aBars.add(bar);
		}
	}

	public void weeklyGraph(int year, int month, int dayinput) {
		final ArrayList<Bar> aBars = new ArrayList<Bar>();
		barGraph.setBars(aBars);
		barGraph.setShowAxisLabel(true);
		barGraph.setShowAxis(false);
		barGraph.setShowPopup(true);

		ArrayList<Bars> bars = new ArrayList<Bars>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DATE, FILTER_DAY - 6);
		cal.set(Calendar.MONTH, FILTER_MONTH);
		int currentD = cal.get(Calendar.DATE);

		for (int i = 0; i < 7; i++) {
			cal.set(Calendar.DATE, currentD + i);
			String day = new SimpleDateFormat("dd/MM/yy").format(cal.getTime());
			Bars bm;
			if (i == 0 || i == 7 - 1)
				bm = new Bars(day + "");
			else
				bm = new Bars("");
			// bm.sum = 10
			bars.add(bm);
		}

		barGraph.setMinimumHeight(20);
		for (int j = 0; j < expenses.size(); j++) {
			String monthstr = new SimpleDateFormat("M").format(expenses.get(j)
					.getDate());
			String yearstr = new SimpleDateFormat("yyyy").format(expenses
					.get(j).getDate());
			String dayrstr = new SimpleDateFormat("dd").format(expenses.get(j)
					.getDate());
			Calendar calendarExpense = Calendar.getInstance();
			calendarExpense.setTime(expenses.get(j).getDate());

			Calendar calendarStart = Calendar.getInstance();
			calendarStart.set(Calendar.MONTH, FILTER_MONTH);
			calendarStart.set(Calendar.DATE, FILTER_DAY - 6);

			Calendar calendarEnd = Calendar.getInstance();
			calendarEnd.set(Calendar.MONTH, FILTER_MONTH);
			calendarEnd.set(Calendar.DATE, FILTER_DAY);
			if (calendarStart.before(calendarExpense)
					&& calendarEnd.after(calendarExpense)) {
				for (int i = 0; i < bars.size(); i++) {
					Calendar comp = Calendar.getInstance();
					comp.set(Calendar.DATE, FILTER_DAY - 1);

					if (((FILTER_DAY - 6) + i) == Integer.valueOf(dayrstr)) {
						bars.get(i).sum += expenses.get(j).getAmount();
					}
				}
			}

		}

		for (int i = 0; i < bars.size(); i++) {
			Bar bar = new Bar();
			bar.setLabelColor(Color.rgb(207, 207, 207));
			bar.setColor(getActivity().getResources().getColor(R.color.green));
			bar.setSelectedColor(Color.rgb(207, 207, 207));
			bar.setName(bars.get(i).name);
			bar.setValue(bars.get(i).sum);

			aBars.add(bar);
		}
	}

	public void addItemsOnSpinner2(View v) {
		filterList_YWM.clear();
		spinnerTime = (Spinner) v.findViewById(R.id.spinner2);
		spinnerTime.setFocusable(false);
		spinnerTime.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				setUserTouchSpiner(true);
				return false;
			}
		});

		Calendar cal = Calendar.getInstance();
		FILTER_DAY = cal.get(Calendar.DATE);
		FILTER_MONTH = cal.get(Calendar.MONTH);
		FILTER_YEAR = cal.get(Calendar.YEAR);
		int year;
		switch (FILTER_TYPE) {
		case YEARLY_FILTER_TYPE:

			for (int i = 0; i < 3; i++) {
				year = cal.get(Calendar.YEAR);
				filterList_YWM.add((year - i) + "");
			}
			break;
		case MONTHLY_FILTER_TYPE:
			int month;
			month = cal.get(Calendar.MONTH);
			String months;
			int current_pos = 11 - month;
			for (int i = 0; i < 12 - current_pos; i++) {
				cal.set(Calendar.MONTH, month - i);
				months = new SimpleDateFormat("MMMM").format(cal.getTime());
				filterList_YWM.add(months);
			}
			break;
		case WEEKLY_FILTER_TYPE:
			cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			int before = cal.get(Calendar.DATE);
			cal.set(Calendar.DATE, before+14);
			int currentD = cal.get(Calendar.DATE);

			for (int i = 0; i < 4; i++) {
				cal.set(Calendar.DATE, currentD + (i * -7));
				String start_day = new SimpleDateFormat("dd MMM").format(cal
						.getTime());
				Calendar c2 = Calendar.getInstance();
				c2.set(Calendar.DATE, currentD + ((i + 1) * -7) + 1);
				String end_day = new SimpleDateFormat("dd MMM").format(c2
						.getTime());
				filterList_YWM.add(end_day + " -" + start_day);//
			}

			break;

		default:
			break;
		}

		ArrayAdapter<String> dataAdapter_filterList_YWM = new ArrayAdapter<String>(
				v.getContext(), android.R.layout.simple_spinner_item,
				filterList_YWM);
		dataAdapter_filterList_YWM
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerTime.setAdapter(dataAdapter_filterList_YWM);
		spinnerTime.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Calendar c = Calendar.getInstance();
				if (FILTER_TYPE == YEARLY_FILTER_TYPE) {
					FILTER_YEAR = Integer.valueOf(filterList_YWM.get(arg2));
				}
				if (FILTER_TYPE == MONTHLY_FILTER_TYPE) {
					FILTER_YEAR = c.get(Calendar.YEAR);
					FILTER_MONTH = c.get(Calendar.MONTH) - arg2;
					Calendar c2 = (Calendar) c.clone();
					c2.set(Calendar.MONTH, FILTER_MONTH);
					String monthstr = new SimpleDateFormat("MMM").format(c2
							.getTime());
				}
				if (FILTER_TYPE == WEEKLY_FILTER_TYPE) {
					FILTER_YEAR = c.get(Calendar.YEAR);
					c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
					int before = c.get(Calendar.DATE);
					c.set(Calendar.DATE, before+14);
					int currentD = c.get(Calendar.DATE);
					c.set(Calendar.DATE, currentD + (arg2 * -7));
					FILTER_DAY = c.get(Calendar.DATE);
					FILTER_MONTH = c.get(Calendar.MONTH);
					Toast.makeText(getActivity(),
							FILTER_DAY + " | " + FILTER_MONTH,
							Toast.LENGTH_SHORT).show();
				}
				initGraphView(FILTER_TYPE, FILTER_YEAR, FILTER_MONTH,
						FILTER_DAY);
				if (userTouchSpiner)
					getCloudExpenses(getActivity());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		setUserTouchSpiner(false);
		if (isVisibleToUser) {
			if (StaticData.getInstance().isExpenseListEdited()) {
				StaticData.getInstance().setExpenseListEdited(false);
				if (getActivity() != null) {
					getCloudExpenses(getActivity());
				}
			}
		}
	}

	public void addItemsOnSpinner1(View v) {

		spinnerType = (Spinner) v.findViewById(R.id.spinner1);
		spinnerType.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				setUserTouchSpiner(true);
				return false;
			}
		});
		List<String> list = new ArrayList<String>();
		list.add("Yearly");
		list.add("Monthly");
		list.add("Weekly");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
				v.getContext(), android.R.layout.simple_spinner_item, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerType.setAdapter(dataAdapter);
		spinnerType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				FILTER_TYPE = arg2;
				addItemsOnSpinner2(listtop);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				StaticData.getInstance().setExpenseListEdited(false);
			}
		});

	}

	public boolean isUserTouchSpiner() {
		return userTouchSpiner;
	}

	public void setUserTouchSpiner(boolean userTouchSpiner) {
		this.userTouchSpiner = userTouchSpiner;
	}

	public class GetAllEpensessAsinq extends AsyncTask<Void, Void, Void> {
		Context context;
		ProgressDialog mProgressDialog;
		private boolean running = true;

		ParseQuery<ParseObject> query;

		public GetAllEpensessAsinq(Context context,
				ParseQuery<ParseObject> query) {
			this.context = context;
			this.query = query;
			totalExpense = 0;
			totalIncome = 0;

		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			running = false;
			mProgressDialog.cancel();
			super.onCancelled();
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setTitle("Data loading");
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (running) {

				try {
					parseExspenses = query.find();
				} catch (ParseException e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}
			} else {
				mProgressDialog.cancel();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			// expenses.clear();
			if (running) {
				super.onPostExecute(result);
				totalIncome = 0;
				totalExpense = 0;
				expenses.clear();
				for (ParseObject expensedata : parseExspenses) {
					Expenses expense = Expenses
							.mapExpensesObjectToClass(expensedata);

					// db.addExenses(expense);
					expenses.add(expense);
					if (expense.getType() == 1)
						totalIncome += expense.getAmount();
					else
						totalExpense += expense.getAmount();
				}
				adapter.notifyDataSetChanged();
				initGraphView(FILTER_TYPE, FILTER_YEAR, FILTER_MONTH,
						FILTER_DAY);
				setupTotalExpenses();
				StaticData.getInstance().setExpenseListEdited(false);
			}
			mProgressDialog.cancel();

		}
	}

	@Override
	public void update(Observable observable, Object data) {
		// TODO Auto-generated method stub
		getCloudExpenses(getActivity());
	}

}