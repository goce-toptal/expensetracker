package com.example.expensestrackeractionbar.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.expensestrackeractionbar.R;

public class AddIncome extends AddType{
	
	public AddIncome() {
		// TODO Auto-generated constructor stub
	}
	
	public static AddIncome newInstance(String title) {
		AddIncome pageFragment = new AddIncome();
		Bundle bundle = new Bundle();
		bundle.putString("title", title);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.add_expense, container, false);		
		initView();
		add.setBackgroundColor(rootView.getContext().getResources().getColor(R.color.purple));
		add.setText(resurses.getString(R.string.add_income));
		add.setOnClickListener(this);
		return rootView;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
			// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.actionExpenses:
				addExpense(v,1);
				
				break;
			}
	}

}
