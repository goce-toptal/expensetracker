package com.example.expensestrackeractionbar.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginActivity extends Activity implements OnClickListener{
	
	EditText userText;
	EditText passwordText;
	Button loginButton;
	Button signupButton;
	String username;
    String password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		initView();
	}
	
	public void  initView() {
		userText = (EditText) findViewById(R.id.username);
		passwordText = (EditText) findViewById(R.id.password);
		loginButton = (Button) findViewById(R.id.loginbtn);
		signupButton = (Button) findViewById(R.id.signupbtn);
		loginButton.setOnClickListener(this);
		signupButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.loginbtn:
			logInUser();
			break;
		case R.id.signupbtn:
			Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(intent);
            finish();
			break;

		default:
			break;
		}
	}
	
	public void logInUser(){
		username = userText.getText().toString().trim();
        password = passwordText.getText().toString().trim();
        
        if (!Utility.HaveNetworkConnection(this)) {
			Utility u = new Utility(this);
			u.createInternetDisabledAlert();
			return;
		}
        
        if(!Utility.isNotEmptyValid(username))
        {
        	userText.setError("Incorect usrername");
        	return;
        }
        ParseUser.logInInBackground(username, password, new LogInCallback() {
			@Override
			public void done(ParseUser user, ParseException e) {
				// TODO Auto-generated method stub
				if(user != null){
					StaticData.getInstance().setExpenseListEdited(true);
					Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		            startActivity(intent);
		            
		            finish();
				}else{
					Toast.makeText(LoginActivity.this, "hm i'm unable to log in", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
}
