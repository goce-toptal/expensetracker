package com.example.expensestrackeractionbar.ui;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.dom.Expenses;
import com.example.expensestrackeractionbar.utility.MyDateFormatter;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class EditExpensesActivity extends Activity implements OnClickListener,OnFocusChangeListener {

	EditText comment, ammount, description,date,time;
	RadioGroup radioType;
	Button edit;
	String username, password, email;
	String expensesId;
	Expenses expense;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_expense);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		initView();
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			expensesId = bundle.getString(Expenses.OBJECT_ID);
			if (!Utility.HaveNetworkConnection(this)) {
				Utility u = new Utility(this);
				u.createInternetDisabledAlert();
				return;
			}
			getCloudExpense();
		}
	}
	
	public void getCloudExpense(){
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(Expenses.TABBLE_NAME);
		query.getInBackground(expensesId, new GetCallback<ParseObject>() {
			public void done(ParseObject object, ParseException e) {
				if (e == null) {
					expense = Expenses.mapExpensesObjectToClass(object);
					comment.setText(expense.getComent());
					description.setText(expense.getDescription());
					ammount.setText(expense.getAmount() + "");
					String defaultdate = MyDateFormatter.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat().format(expense.getDate());
					String defaulttime = MyDateFormatter.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat().format(expense.getDate());
					date.setText(defaultdate);
					time.setText(defaulttime);
					((RadioButton) radioType.getChildAt(expense.getType())).setChecked(true);
				} else {
					
				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// Respond to the action bar's Up/Home button
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void initView() {
		comment = (EditText) findViewById(R.id.comment);
		ammount = (EditText) findViewById(R.id.ammount);
		description = (EditText) findViewById(R.id.description);
		date = (EditText) findViewById(R.id.dateEdit);
		time = (EditText) findViewById(R.id.timeEdit);
	
		edit = (Button) findViewById(R.id.actionExpenses);
		edit.setText(getResources().getString(R.string.edit_expense));
		radioType = (RadioGroup) findViewById(R.id.radioType);
		radioType.setVisibility(RadioGroup.VISIBLE);
		Calendar c = Calendar.getInstance();
		String defaultdate = MyDateFormatter.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat().format(c.getTime());
		String defaulttime = MyDateFormatter.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat().format(c.getTime());
		date.setText(defaultdate);
		time.setText(defaulttime);
		edit.setOnClickListener(this);
		date.setOnClickListener(this);
		date.setOnFocusChangeListener(this);
		time.setOnClickListener(this);
		time.setOnFocusChangeListener(this);
	}
	
	public void updateExpene(View v){
		if (!Utility.HaveNetworkConnection(this)) {
			Utility u = new Utility(this);
			u.createInternetDisabledAlert();
			return;
		}
		Expenses expense = new Expenses();
		expense.setId(expensesId);
		expense.setComent(comment.getText().toString().trim());
		
		double value = 0.00;
		double valueMultiplay = 1.00;
		String descriptionText = "";
		
		radioType.getCheckedRadioButtonId();
		int type=0;
		if(radioType.getCheckedRadioButtonId() == R.id.radioIncome)
			type = 1;
		try {
			value = Double.valueOf(ammount.getText().toString().trim());
		} catch (Exception e) {
				ammount.setError("Enter amount");
			return;
		}
		expense.setAmount(value);
		descriptionText = description.getText().toString().trim();
		if(descriptionText.equals(""))
			descriptionText = "No description";
		expense.setDescription(descriptionText);
		Date d;
		try {
			d = MyDateFormatter.getInstance(MyDateFormatter.DATE_AND_TIME_FORMAT).getDateFormat().parse(date.getText().toString()+" "+time.getText().toString());
			expense.setDate(d);
			expense.setTime(d.getTime());
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		expense.setUser(ParseUser.getCurrentUser());
		expense.setType(type);
		StaticData.getInstance().setExpenseListEdited(true);
		Expenses.updateExpenses(v.getContext(),expense);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.actionExpenses:
			updateExpene(v);
			break;
		case R.id.dateEdit:
			Utility.showDatePickerDialog((EditText)v);
			break;
		case R.id.timeEdit:
			 Utility.showTimePickerDialog((EditText)v);
			break;
		default:
			break;
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.dateEdit:
			if(hasFocus)
			Utility.showDatePickerDialog((EditText)v);
			break;
		case R.id.timeEdit:
			if(hasFocus)
			Utility.showTimePickerDialog((EditText)v);
			break;

		default:
			break;
		}
	}

}
