package com.example.expensestrackeractionbar.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.expensestrackeractionbar.R;

public class AddExpenses extends AddType  {

	public AddExpenses() {
		// TODO Auto-generated constructor stub
	}
	
	public static AddExpenses newInstance(String title) {
		AddExpenses pageFragment = new AddExpenses();
		Bundle bundle = new Bundle();
		bundle.putString("title", title);
		pageFragment.setArguments(bundle);
		return pageFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		rootView = inflater.inflate(R.layout.add_expense, container, false);
		initView();
		add.setOnClickListener(this);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		switch (v.getId()) {
		case R.id.actionExpenses:
			addExpense(v, 0);
			break;

		default:
			break;
		}
	}
	
	

}
