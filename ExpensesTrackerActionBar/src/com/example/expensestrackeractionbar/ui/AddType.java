package com.example.expensestrackeractionbar.ui;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.dom.Expenses;
import com.example.expensestrackeractionbar.utility.MyDateFormatter;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.ParseUser;

public class AddType extends Fragment implements OnClickListener,
		OnFocusChangeListener {

	protected View rootView;
	EditText comment, ammount, description, date, time;
	RadioGroup radioType;
	Button add;
	String username, password, email;
	String expensesId;
	Expenses expense;
	Calendar c;
	Resources resurses;
	AddTypeCalback updateCalback;
	
	public AddType() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		resurses = this.getResources();
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public void initView() {
		comment = (EditText) rootView.findViewById(R.id.comment);
		ammount = (EditText) rootView.findViewById(R.id.ammount);
		description = (EditText) rootView.findViewById(R.id.description);
		date = (EditText) rootView.findViewById(R.id.dateEdit);
		time = (EditText) rootView.findViewById(R.id.timeEdit);
		add = (Button) rootView.findViewById(R.id.actionExpenses);
		date.setOnClickListener(this);
		time.setOnClickListener(this);
		time.setOnFocusChangeListener(this);
		date.setOnFocusChangeListener(this);
		c = Calendar.getInstance();
		String defaultdate = MyDateFormatter
				.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat()
				.format(c.getTime());
		String defaulttime = MyDateFormatter
				.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat()
				.format(c.getTime());
		date.setText(defaultdate);
		time.setText(defaulttime);
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.dateEdit:
			if (hasFocus)
				Utility.showDatePickerDialog((EditText) v);
			break;
		case R.id.timeEdit:
			if (hasFocus)
				Utility.showTimePickerDialog((EditText) v);
			break;

		default:
			break;
		}
	}

	public void addExpense(View v, int type) {
		if (!Utility.HaveNetworkConnection(getActivity())) {
			Utility u = new Utility(getActivity());
			u.createInternetDisabledAlert();
			return;
		}
		try {
			Expenses expense = new Expenses();
			expense.setComent(comment.getText().toString().trim());
			double value = 0.00;
			String descriptionText = "";
			
			descriptionText = description.getText().toString().trim();
			if(descriptionText.equals(""))
				descriptionText = "No description";
			
			expense.setDescription(descriptionText);
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			try {
				value = Double.valueOf(ammount.getText().toString().trim());
			} catch (Exception e) {
				ammount.setError("Enter amount");
				return;
			}
			Date d = MyDateFormatter
					.getInstance(MyDateFormatter.DATE_AND_TIME_FORMAT)
					.getDateFormat()
					.parse(date.getText().toString() + " "
							+ time.getText().toString());
			expense.setDate(d);
			expense.setUser(ParseUser.getCurrentUser());
			expense.setTime(d.getTime());
			expense.setType(type);
			expense.setAmount(value);
			Expenses.AddNewExpenses(expense);
			updateCalback = (MainActivity) getActivity();
			updateCalback.updatedExpenseList();
			

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {

		case R.id.dateEdit:
			Utility.showDatePickerDialog((EditText) v);
			break;
		case R.id.timeEdit:
			Utility.showTimePickerDialog((EditText) v);
			break;
		default:
			break;
		}
	}
	
 public static interface AddTypeCalback{
	public void updatedExpenseList();
}
}
