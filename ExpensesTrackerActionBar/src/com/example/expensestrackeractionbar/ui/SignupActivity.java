package com.example.expensestrackeractionbar.ui;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignupActivity extends Activity implements OnClickListener{
	
	EditText usernameText,person_nameText,person_surnameText,passwordText,confirmpasswordText,emailText;
	Button signupButton,loginButton;
	String username,password,email,person_name,person_surname;
	boolean userNotExsist = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		initView();
	}
	
	public void initView() {
		usernameText = (EditText) findViewById(R.id.username);
		passwordText = (EditText) findViewById(R.id.password);
		person_nameText = (EditText) findViewById(R.id.name);
		person_surnameText = (EditText) findViewById(R.id.surname);
		confirmpasswordText = (EditText) findViewById(R.id.confirmPassword);
		emailText = (EditText) findViewById(R.id.email);
		signupButton = (Button) findViewById(R.id.signupbtn);
		loginButton = (Button) findViewById(R.id.loginbtn);
		signupButton.setOnClickListener(this);
		loginButton.setOnClickListener(this);
	}
	
	public void clearValidationErrors(){
		Utility.clearValidationErros(usernameText);
		Utility.clearValidationErros(passwordText);
		Utility.clearValidationErros(person_nameText);
		Utility.clearValidationErros(person_surnameText);
		Utility.clearValidationErros(confirmpasswordText);
		Utility.clearValidationErros(emailText);
		
	}
	
	public boolean validateEditTextBoxes(){
		boolean ok=true;
		
		if(!Utility.validateEditTextIsEmpty(usernameText, ""))
			ok= false;
		if(!Utility.validateEditTextIsEmpty(passwordText, ""))
			ok= false;
//		if(!Utility.validateEditTextIsEmpty(person_nameText, ""))
//			ok= false;
//		if(!Utility.validateEditTextIsEmpty(person_surnameText, ""))
//			ok= false;
		if(!Utility.validateEditTextIsEmpty(confirmpasswordText, ""))
			ok= false;
		if(!Utility.validateEditTextIsEmpty(emailText, ""))
			ok= false;
		if(!Utility.validateIfEmailEntered(emailText, ""))
			ok=false;
		if(!Utility.confirmPasswordCheck(passwordText,confirmpasswordText,""))
			ok=false;
			
		return ok;
	}
	
	public boolean checkIfUserExists(final EditText username){
		clearValidationErrors();
		if (!Utility.HaveNetworkConnection(this)) {
			Utility u = new Utility(this);
			u.createInternetDisabledAlert();
			return false;
		}
		final boolean ok = true;
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo("username", username.getText().toString().trim());
		query.setLimit(1);
		final ProgressDialog mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setTitle("Data loading");
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.show();
		query.findInBackground(new FindCallback<ParseUser>() {
		  public void done(List<ParseUser> objects, ParseException e) {
		    if (e == null) {
		        // The query was successful.
		    	if(objects.size()>0){
		    		username.setError("this username exist");
		    	}else{
		    		signUpUser();
		    	}
		    } else {
		    	
		    }
		    mProgressDialog.cancel();
		  }
		});
		return userNotExsist;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i;
		switch (v.getId()) {
		case R.id.loginbtn:
			i = new Intent(getApplicationContext(),LoginActivity.class);
			startActivity(i);
			finish();
			break;
		case R.id.signupbtn:
			checkIfUserExists(usernameText);
			break;

		default:
			break;
		}
	}
	
	public void signUpUser(){
		
		if (!Utility.HaveNetworkConnection(this)) {
			Utility u = new Utility(this);
			u.createInternetDisabledAlert();
			return;
		}
		if(!validateEditTextBoxes())
			return;
		
		username = usernameText.getText().toString().trim();
		password = passwordText.getText().toString().trim();
		email = emailText.getText().toString().trim();
		person_name = person_nameText.getText().toString().trim();
		person_surname = person_surnameText.getText().toString().trim();
		
		ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.put("name", person_name);
        user.put("surname", person_surname);
        user.setEmail(email);
        
        user.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				// TODO Auto-generated method stub
				StaticData.getInstance().setExpenseListEdited(true);
				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
				finish();
			}
		});
	}
}
