package com.example.expensestrackeractionbar.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.dom.Expenses;
import com.example.expensestrackeractionbar.ui.EditExpensesActivity;
import com.example.expensestrackeractionbar.ui.ViewExpensesActivity;
import com.example.expensestrackeractionbar.utility.Constants;
import com.example.expensestrackeractionbar.utility.Utility;
import com.parse.ParseObject;



public class ExpensesAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<Expenses> expenses = new LinkedList<Expenses>();
	private Context context;
	private int openid = -1;
	private boolean fromAdditional = false;
	int bluecolor;
	int greencolor;
	
	public ExpensesAdapter(Context context,List<Expenses> expenses){
		this.context = context;
		inflater = LayoutInflater.from(context);
		this.expenses = expenses;
		fromAdditional = false;
		bluecolor = context.getResources().getColor(R.color.darkblue);
		greencolor = context.getResources().getColor(R.color.green);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return expenses.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return expenses.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 final ViewHolder holder;
		 final Expenses expense = expenses.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.expenses_list_item, null);
			holder = new ViewHolder();
			initilzeViews(holder,convertView);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		String day = new SimpleDateFormat("d").format(expense.getDate());
		if(position%2==0){
			
			holder.hiddenpart.setBackgroundColor(bluecolor);
			holder.itemHolder.setBackgroundColor(bluecolor);
			holder.date.setBackground(context.getResources().getDrawable(R.drawable.date_border_blue));
			holder.year.setTextColor(bluecolor);
			holder.date.setTextColor(bluecolor);
			holder.month.setTextColor(bluecolor);
		}else{
			
			holder.hiddenpart.setBackgroundColor(greencolor);
			holder.itemHolder.setBackgroundColor(greencolor);
			holder.date.setBackground(context.getResources().getDrawable(R.drawable.date_border_green));
			holder.year.setTextColor(greencolor);
			holder.date.setTextColor(greencolor);
			holder.month.setTextColor(greencolor);
		}
		
		if(expense.getType()==0){
			holder.type.setImageResource(android.R.drawable.stat_sys_upload_done);
		}else{
			holder.type.setImageResource(android.R.drawable.stat_sys_download_done);
		}
	
		holder.amount.setText(expense.getAmount()+" $");
		holder.description.setText(expense.getDescription().trim());
		holder.comment.setText(expense.getComent().trim());
		
		holder.date.setText(day+"");
		holder.pos = position;
		
		String month = new SimpleDateFormat("MMM").format(expense.getDate());
		holder.month.setText(month+"");
		
		String year = new SimpleDateFormat("yyyy").format(expense.getDate());
		holder.year.setText(year);
		holder.itemHolder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openAdditionalPart(position);
			}
		});
		
		holder.edit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i  = new Intent(context,EditExpensesActivity.class);
				i.putExtra(Expenses.OBJECT_ID, expense.getId());
				context.startActivity(i);
				
			}
		});
		
		holder.remove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				removeListItem(holder.mainItemHolder,position); 
			}
		});
		
		
		holder.view.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i  = new Intent(context,ViewExpensesActivity.class);
				i.putExtra(Expenses.OBJECT_ID, expense.getId());
				context.startActivity(i);
			}
		});
		
		Date df = new java.util.Date(expense.getTime());
		String vv = new SimpleDateFormat(" kk:mm").format(df);
		holder.time.setText(vv+"");
		
		checkState(position,holder);
		
		return convertView;
	}
	
	private void initilzeViews(ViewHolder holder,View convertView) {
		// TODO Auto-generated method stub
		holder.itemHolder = (RelativeLayout) convertView.findViewById(R.id.itemHolder);
		holder.time = (TextView) convertView.findViewById(R.id.time);
		holder.amount = (TextView) convertView.findViewById(R.id.amount);
		holder.description = (TextView) convertView.findViewById(R.id.descripton);
		holder.comment = (TextView) convertView.findViewById(R.id.comment);
		holder.date = (TextView) convertView.findViewById(R.id.date);
		holder.month  = (TextView) convertView.findViewById(R.id.month);
		holder.hiddenpart = (LinearLayout) convertView.findViewById(R.id.hidenpart);
		holder.edit = (ImageView) convertView.findViewById(R.id.edititem);
		holder.remove = (ImageView) convertView.findViewById(R.id.removeitem);
		holder.mainItemHolder = (LinearLayout) convertView.findViewById(R.id.mainItemHolder);
		holder.type = (ImageView) convertView.findViewById(R.id.typeImage);
		holder.year = (TextView) convertView.findViewById(R.id.year);
		holder.view = (ImageView) convertView.findViewById(R.id.viewitem);
	}

	static class ViewHolder {
		LinearLayout mainItemHolder;
		LinearLayout hiddenpart;
		RelativeLayout itemHolder;
		ImageView type;
		TextView time;
		TextView amount;
		TextView description;
		TextView comment;
		TextView date;
		TextView month;
		ImageView remove;
		ImageView edit;
		ImageView view;
		TextView year;
		int state=0;
		int pos =-1;
	}
	
	public void openAdditionalPart(int position){
		openid = position;
		fromAdditional = true;
		notifyDataSetChanged();
	}
	
	public void checkState(int position,ViewHolder holder){
		int width =holder.hiddenpart.getMeasuredWidth();
		if(!fromAdditional)
			return;
		if((openid == position && holder.state == 0) || (expenses.size()==1 && holder.state == 1)){
			 	
		      ObjectAnimator animation1 = ObjectAnimator.ofFloat(holder.hiddenpart, "x",width, Utility.convertDpToMetric(context, 5));
		      animation1.setDuration(300);
		      animation1.start();
		      holder.hiddenpart.setVisibility(View.VISIBLE);
		      holder.state = 1;
		}else
		{
		      ObjectAnimator animation1 = ObjectAnimator.ofFloat(holder.hiddenpart, "x",Utility.convertDpToMetric(context, 5),width);
		      animation1.setDuration(200);
		      animation1.start();
		      holder.hiddenpart.setVisibility(View.GONE);
		      holder.state = 0;
		}
	}
	
	protected void removeListItem(View rowView,final int pos) {
        final Animation animation = AnimationUtils.loadAnimation(
                rowView.getContext(), android.R.anim.slide_out_right);
        rowView.startAnimation(animation);
        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            public void run() {
            	int positon = pos;
            	
                ParseObject.createWithoutData(Expenses.TABBLE_NAME, expenses.get(positon).getId()).deleteEventually();
                expenses.remove(positon);
                openid = -1;
                notifyDataSetChanged();
                Constants.expenseObservable.announceUpdates();
            }
        }, 400);
}

	
}
