package com.example.expensestrackeractionbar.utility;

import java.util.Observable;

public class ExpenseObservable  extends Observable {
	 public void announceUpdates() {
	        setChanged();
	        notifyObservers();
	   }   
}
