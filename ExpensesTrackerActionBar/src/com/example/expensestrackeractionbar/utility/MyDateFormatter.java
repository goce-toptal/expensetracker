package com.example.expensestrackeractionbar.utility;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class MyDateFormatter {
	private static final MyDateFormatter instance = new MyDateAndTimeFormatter();
	private static final MyDateFormatter instance1 = new MyDateFormatter();
	private static final MyDateFormatter instance2 = new MyTimeFormatter();
	public static final int DATE_AND_TIME_FORMAT = 0;
	public static final int DATE_FORMAT = 1;
	public static final int TIME_FROMAT = 2;
	public  SimpleDateFormat sdf;
	
	public MyDateFormatter() {
		sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
	}

	public SimpleDateFormat getDateFormat() {
		return sdf;
	}
	
	public static MyDateFormatter getInstance(int type) {
		if(type == DATE_AND_TIME_FORMAT)
			return instance;
		if(type == DATE_FORMAT)
			return instance1;
		
		return instance2;
	}
}

class MyDateAndTimeFormatter extends MyDateFormatter{
	MyDateAndTimeFormatter() {
		sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
	}
}

class MyTimeFormatter extends MyDateFormatter{
	MyTimeFormatter() {
		sdf = new SimpleDateFormat("kk:mm:ss", Locale.ENGLISH);
	}
}