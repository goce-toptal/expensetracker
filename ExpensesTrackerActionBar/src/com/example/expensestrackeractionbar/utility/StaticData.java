package com.example.expensestrackeractionbar.utility;


public class StaticData {

	private static StaticData instance = null;
	private boolean expenseEdited = true;

	protected StaticData() {
	}

	public static StaticData getInstance() {
		if (instance == null) {
			instance = new StaticData();
		}
		return instance;
	}

	public boolean isExpenseListEdited() {
		return expenseEdited;
	}

	public void setExpenseListEdited(boolean expenseEdited) {
		this.expenseEdited = expenseEdited;
	}

}
