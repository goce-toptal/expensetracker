package com.example.expensestrackeractionbar.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import android.R.bool;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.expensestrackeractionbar.R;
import com.example.expensestrackeractionbar.ui.LoginActivity;
import com.parse.ParseUser;

public class Utility {

	private Context ctx;

	/**
	 * Constructor of the Model class which initializes the activity context.
	 * 
	 * @param context
	 */
	public Utility(Context context) {
		ctx = context;
	}
	
	public static final String md5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public  static boolean isValidEmail(CharSequence target) {
		return target != null
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}
	
	public  static boolean isNotEmptyValid(CharSequence str) {
		return str.length() > 2;
	}
	
	public  static boolean paswordMatching(CharSequence str1,CharSequence str2) {
		return str1.equals(str2);
	}
	
	
	
	public static boolean validateEditTextIsEmpty(EditText v,String errormesage){
		if(errormesage.isEmpty())
			errormesage = "This field is less then 3 letter";
		
		boolean ok=true;
		if(!isNotEmptyValid(v.getText().toString().trim()))
		{
			ok = false;
			v.setError(errormesage);
		}
			
		return ok;
	}
	
	public static void clearValidationErros(EditText v){
		v.setError(null);
	}
	
	public static boolean confirmPasswordCheck(EditText v1,EditText v2,String errormesage){
		if(errormesage.isEmpty())
			errormesage = "Paswords are not same";
		
		boolean ok=true;
		if(!paswordMatching(v1.getText().toString().trim(),v2.getText().toString().trim()))
		{
			ok = false;
			v2.setError(errormesage);
		}
			
		return ok;
	}
	
	public static boolean validateIfEmailEntered(EditText v,String errormesage){
		if(errormesage.isEmpty())
			errormesage = "This is not email pattern";
		
		boolean ok=true;
		if(!isValidEmail(v.getText().toString().trim()))
		{
			ok = false;
			v.setError(errormesage);
		}
			
		return ok;
	}

	public static void userLogout(Context context) {
		// TODO Auto-generated method stub
		ParseUser.logOut();
		Intent i = new Intent(context, LoginActivity.class);
		context.startActivity(i);
	}
	
	public static boolean HaveNetworkConnection(Context ctx) {
		boolean HaveConnectedWifi = false;
		boolean HaveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					HaveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					HaveConnectedMobile = true;
		}
		return HaveConnectedWifi || HaveConnectedMobile;
	}
	
	public void createInternetDisabledAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setMessage(R.string.internet_disabled_alert)
				.setIcon(R.drawable.ic_launcher)
				.setTitle(R.string.app_name)
				.setCancelable(true)
				.setPositiveButton(R.string.internet_options,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int id) {
								showNetOptions();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void showNetOptions() {
		Intent netOptionsIntent = new Intent(
				android.provider.Settings.ACTION_WIRELESS_SETTINGS);
		ctx.startActivity(netOptionsIntent);
	}
	
	public static int convertDpToMetric(Context context,int dp){
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}
	
	
	public static void showDatePickerDialog(final EditText v){
		final Calendar c = Calendar.getInstance();
		final Calendar c2 = Calendar.getInstance();
		int mYear,mMonth,mDay;
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		 
		DatePickerDialog dateDialog = new DatePickerDialog(v.getContext(),
				new OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						// TODO Auto-generated method stub
						c2.set(Calendar.YEAR, year);
						c2.set(Calendar.MONTH, monthOfYear);
						c2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
						v.setText(MyDateFormatter.getInstance(MyDateFormatter.DATE_FORMAT).getDateFormat().format(c2.getTime()));
						
					}
				}, mYear, mMonth, mDay);
		dateDialog.show();
	}
	
	public static Calendar showTimePickerDialog(final EditText v){
		final Calendar c = Calendar.getInstance();
		final Calendar c2 = Calendar.getInstance();
		int hourOfDay,minute;
		hourOfDay = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);
		 
		TimePickerDialog timeDialog = new TimePickerDialog(v.getContext(), new OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				c2.set(Calendar.HOUR_OF_DAY, hourOfDay);
				c2.set(Calendar.MINUTE, minute);
				v.setText(MyDateFormatter.getInstance(MyDateFormatter.TIME_FROMAT).getDateFormat().format(c2.getTime()));
			
			}
		}, hourOfDay, minute, false);
		timeDialog.show();
		
		return c2;
	}


	
}
