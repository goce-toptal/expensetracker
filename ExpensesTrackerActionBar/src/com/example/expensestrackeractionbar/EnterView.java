package com.example.expensestrackeractionbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.expensestrackeractionbar.ui.LoginActivity;
import com.example.expensestrackeractionbar.ui.MainActivity;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseUser;

public class EnterView extends Activity {
	
	TextView getdata;
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        if (ParseAnonymousUtils.isLinked(ParseUser.getCurrentUser())) {
            Intent intent = new Intent(EnterView.this,LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                Intent intent = new Intent(EnterView.this,MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(EnterView.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
	}
}
