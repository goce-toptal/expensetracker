package com.example.expensestrackeractionbar.dom;

import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.expensestrackeractionbar.ui.MainActivity;
import com.example.expensestrackeractionbar.ui.AddType.AddTypeCalback;
import com.example.expensestrackeractionbar.utility.StaticData;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public final class Expenses{

	private String id;
	long time;
	double amount;
	String description;
	String coment;
	Date date = new Date();
	ParseUser user;
	String userObjectId;
	int type = 0;

	public static final String TIME = "time";
	public static final String AMOUNT = "amount";
	public static final String DESCRIPTION = "description";
	public static final String COMMENT = "comment";
	public static final String DATE = "date";
	public static final String USER = "user";
	public static final String TYPE = "type";
	public static final String TABBLE_NAME = "Expenses";
	public static final String OBJECT_ID = "objectId";	

	
	public static enum Tables {
		OBJECT_ID, AMOUNT, COMMENT, DATE, DESCRIPTION, TIME, USER, TABBLE_NAME ;
		public int get(Tables item) {
			return item.get(item);
		}
	}
	
	
	public Expenses() {
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getComent() {
		return coment;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public ParseUser getUser() {
		return user;
	}

	public void setUser(ParseUser user) {
		this.user = user;
	}
	
	public void setUserOjectId(String userObjectId) {
		// TODO Auto-generated method stub
		this.userObjectId = userObjectId;
	}
	
	public String getUserOjectId() {
		// TODO Auto-generated method stub
		return this.userObjectId;
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public static Expenses mapExpensesObjectToClass(ParseObject expensedata) {
		Expenses expense = new Expenses();
		expense.setAmount(expensedata.getDouble(Expenses.AMOUNT));
		expense.setComent(expensedata.getString(Expenses.COMMENT));
		expense.setDescription(expensedata
				.getString(Expenses.DESCRIPTION));
		expense.setId(expensedata.getObjectId());
		expense.setDate(expensedata.getDate(Expenses.DATE));
		expense.setTime(expensedata.getLong(Expenses.TIME));
		expense.setUserOjectId(expensedata.getString(Expenses.USER));
		expense.setUser(expensedata.getParseUser(Expenses.USER));
		expense.setType(expensedata.getInt(Expenses.TYPE));
		return expense;
	}

	public static void AddNewExpenses(Expenses expense) {

		ParseObject rec = new ParseObject(Expenses.TABBLE_NAME);
		rec.put(Expenses.AMOUNT, expense.getAmount());
		rec.put(Expenses.DESCRIPTION, expense.getDescription());
		rec.put(Expenses.COMMENT, expense.getComent());
		rec.put(Expenses.DATE, expense.getDate());
		rec.put(Expenses.USER, expense.getUser());
		rec.put(Expenses.TIME, expense.getTime());
		rec.put(Expenses.TYPE, expense.getType());
		rec.saveInBackground();
		StaticData.getInstance().setExpenseListEdited(true);
	}

	public static void updateExpenses(final Context context,final Expenses exp) {
		// TODO Auto-generated method stub
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Expenses.TABBLE_NAME);
		// Retrieve the object by id
		query.getInBackground(exp.getId(), new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject expParseObject, ParseException e) {
				// TODO Auto-generated method stub
				if (e == null) {
					expParseObject.put(Expenses.AMOUNT, exp.getAmount());
					expParseObject.put(Expenses.COMMENT, exp.getComent());
					expParseObject.put(Expenses.DESCRIPTION, exp.getDescription());
					expParseObject.put(Expenses.TIME, exp.getTime());
					expParseObject.put(Expenses.DATE, exp.getDate());
					expParseObject.put(Expenses.TYPE, exp.getType());
					expParseObject.saveInBackground();
					StaticData.getInstance().setExpenseListEdited(true);
					Toast.makeText(context, exp.getId()+"", Toast.LENGTH_SHORT).show();
					((Activity)context).finish();
					
				}else{
					Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}
