package com.example.expensestrackeractionbar;

import android.app.Application;

import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class ParseApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();
    
    // Add your initialization code here
    Parse.initialize(this, "grgKY0sDlmKN0y1d9Li5E54zVWt0wCy4JIaxOFQY", "m5P8aa7meBBkfECdMx7E9Ae3AvClNu7tKv9T7Mfy");

    ParseUser.enableAutomaticUser();
    ParseACL defaultACL = new ParseACL();
      
    // If you would like all objects to be private by default, remove this line.
    defaultACL.setPublicReadAccess(true);
    
    ParseACL.setDefaultACL(defaultACL, true);
    
    
  }
}
